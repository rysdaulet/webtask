<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Тестовое задание</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link href="https://fonts.googleapis.com/css?family=Arizonia" rel="stylesheet">

<style type="text/css">
  .quote{
        border: 1px solid #ddd;
        padding: 10px;
        border-radius: 5px;
        font-family: 'Arizonia', cursive;
        font-size: 22px;
        position: relative;
        margin-bottom: 30px;
  }
  .quote span{
      position: absolute;
      right: 3px;
      font-size: 16px;
      color: red;
      top: 5px;
      cursor: pointer;
  }
</style>
</head>
<body ng-app="myApp" ng-controller="myCtrl">

<div class="container">
	<h3>Quotes added</h3>
	<div class="progress">
    	<div class="progress-bar" role="progressbar" aria-valuenow="{{count}}" aria-valuemin="0" aria-valuemax="10" style="width:{{count}}0%">
      	{{count}}/10
    	</div>
  	</div>

  	<div class="row">
  		<div class="col-md-6 col-md-offset-3">
  			<label>Quote</label>
  			<textarea class="form-control" style="margin-bottom: 10px;" ng-model="newquote" rows="3"></textarea>
  			<p class="text-center">
  				<button class="btn btn-primary" ng-click="add()">Add Quote</button>
  			</p>
  		</div>
  	</div>
<div class="panel panel-info">
  <div class="panel-heading">Если не обновляется, попробуйте через <strong>CTRL+F5</strong> (кэшируется)</div>
</div>
    <div class="row" style="margin-top: 20px;">
      <div class="col-md-3" ng-repeat="item in quotes | reverse">
        <div class="quote">{{item.quote}}<span class="glyphicon glyphicon-remove" ng-click="delete($index)"></span></div>
      </div>
    </div>

</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>

<script>
var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope,$http) {
    $scope.quotes = [];
    $scope.count = 0;
    $scope.newquote = '';

    $http.get('quotes.json').then(function(response) {
   		$scope.quotes = response.data;
   		$scope.count = $scope.quotes.length;
	  });

    $scope.add = function() {
      if($scope.newquote!=''){
        if($scope.count<10){
          $scope.quotes.push({"quote": $scope.newquote});
          
        var req = {
          method:'POST',
          url:'write.php',
          headers: {
            'Content-Type':'application/JSON'
          },
          data:{
            quote:$scope.newquote
          }
        };
        $http(req).then( function(data){
          console.log(data);
        });

          $scope.newquote = '';
        }
        else{
          alert('Для добавления новых цитат удалите одну из добавленных.');
        }
        $scope.count = $scope.quotes.length;
      }
      else{
          alert('Пустое поле!');
      }
    };


    $scope.delete = function(index) {
      var id = $scope.quotes.length - index - 1;
      $scope.quotes.splice(id, 1);

      var req = {
          method:'POST',
          url:'delete.php',
          headers: {
            'Content-Type':'application/JSON'
          },
          data:{
            id:id
          }
        };
        $http(req).then( function(data){
          console.log(data);
        });

        $scope.count = $scope.quotes.length;
    };


});


app.filter('reverse', function() {
  return function(items) {
    return items.slice().reverse();
  };
});


</script>


</body>
</html>